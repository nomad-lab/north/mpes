#!/bin/bash
chmod 777 -R .cache
LOG_FILE=generate_requirements.log
mv $LOG_FILE $LOG_FILE.1
exec 3>&1 1>>${LOG_FILE} 2>&1

docker run --rm --env HOME=/home/jovyan --mount type=bind,source="$(pwd)",target=/home/jovyan \
            --platform linux/amd64 --entrypoint bash jupyter/scipy-notebook:2023-04-10 -c \
            "pip install pip-tools; pip-compile --verbose --upgrade --output-file=requirements.jupyter.txt --resolver=backtracking requirements.jupyter.in" \
	     | tee /dev/fd/3

# This is not fully working yet!
docker run --rm --env HOME=/config --mount type=bind,source="$(pwd)",target=/config/requirements \
            --platform linux/amd64 --entrypoint bash gitlab-registry.mpcdf.mpg.de/nomad-lab/nomad-remote-tools-hub/webtop:v0.0.1 -c \
            "apt-get update; apt-get install -y pip git; pip install pip-tools; cd /config/requirements; pip-compile --verbose --upgrade --output-file=requirements.webtop.txt --resolver=backtracking requirements.jupyter.in requirements.webtop.in; chown -R 1023:5050 ." \
	     | tee /dev/fd/3
